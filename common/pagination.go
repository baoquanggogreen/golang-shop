package common

import (
	"gorm.io/gorm"
)

type PagingModel struct {
	Page     int `json:"page"`
	PageSize int `json:"page_size"`
}

type PagingResponse struct {
	Data     interface{} `json:"data"`
	Page     int         `json:"page"`
	PageSize int         `json:"page_size"`
}

func Paginate(page int, page_size int) func(db *gorm.DB) *gorm.DB {
	return func(db *gorm.DB) *gorm.DB {
		if page == 0 {
			page = 1
		}
		switch {
		case page_size > 100:
			page_size = 100
		case page_size <= 0:
			page_size = 10
		}
		offset := (page - 1) * page_size
		return db.Offset(offset).Limit(page_size)
	}
}

func ListResponse(data interface{}, condition interface{}) PagingResponse {
	return PagingResponse{
		Data:     data,
		Page:     condition.(PagingModel).Page,
		PageSize: condition.(PagingModel).PageSize,
	}
}
