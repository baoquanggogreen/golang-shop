package config

import (
	"fmt"
	_ "log"
	"os"

	"github.com/joho/godotenv"
	"gorm.io/driver/postgres"
	"gorm.io/gorm"
	_ "gorm.io/gorm/logger"
)

var DB *gorm.DB

func ConnectDatabase() *gorm.DB {
	godotenv.Load(".env")

	HOST := os.Getenv("HOST")
	USER := os.Getenv("USER")
	PASSWORD := os.Getenv("PASSWORD")
	DB_NAME := os.Getenv("DB_NAME")
	PORT := os.Getenv("PORT")
	dsn := fmt.Sprintf(
		"host=%s user=%s password=%s dbname=%s port=%s sslmode=disable TimeZone=Asia/Ho_Chi_Minh",
		HOST, USER, PASSWORD, DB_NAME, PORT,
	)

	database, err := gorm.Open(postgres.Open(dsn), &gorm.Config{
	// 	Logger: logger.New(
	// 		log.New(os.Stdout, "\r\n", log.LstdFlags),
	// 		logger.Config{
	// 			Colorful: false,
	// 			LogLevel: logger.Info,
	// 		},
	// 	),
	})
	if err != nil {
		panic("Failed to connect to database!")
	}

	DB = database
	return DB
}

func CloseDatabaseConnection(db *gorm.DB) {
	dbSQL, err := db.DB()
	if err != nil {
		panic("Failed to close connection from database")
	}
	dbSQL.Close()
}

func GetDB() *gorm.DB {
	return DB
}
