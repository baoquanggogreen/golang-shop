package admin

import (
	"github.com/gin-gonic/gin"
	"web-service-gin/models"
)

type UserDetailSerializer struct {
	c *gin.Context
	models.User
}

type ListUserSerializer struct {
	c    *gin.Context
	User []models.User
}

type UserDetailResponse struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Fullname string `json:"full_name"`
	Phone    string `json:"phone"`
	Birthday string `json:"birthday"`
	Gender   string `json:"gender"`
	Address  string `json:"address"`
}

func (u *UserDetailSerializer) DetailResponse() UserDetailResponse {
	user := UserDetailResponse{
		Username: u.Username,
		Fullname: u.FullName,
		Email:    u.Email,
		Phone:    u.Phone,
		Birthday: u.Birthday,
		Gender:   u.Gender,
		Address:  u.Address,
	}
	return user
}

func (u *ListUserSerializer) ListResponse() []UserDetailResponse {
	users := []UserDetailResponse{}
	for i := 0; i < len(u.User); i++ {
		users = append(users, UserDetailResponse{
			Username: u.User[i].Username,
			Fullname: u.User[i].FullName,
			Email:    u.User[i].Email,
			Phone:    u.User[i].Phone,
			Birthday: u.User[i].Birthday,
			Gender:   u.User[i].Gender,
			Address:  u.User[i].Address,
		})
	}
	return users
}
