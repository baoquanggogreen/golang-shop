package admin

import (
	"github.com/gin-gonic/gin"
	"web-service-gin/models"
)

type CategoryDetailSerializer struct {
	c        *gin.Context
	Category models.Category
}

type ListCategorySerializer struct {
	c        *gin.Context
	Category []models.Category
}

type CategoryDetailResponse struct {
	ID              uint   `json:"id"`
	Name            string `json:"name"`
	Thumbnail       string `json:"thumbnail"`
	Description     string `json:"description"`
	ProductQuantity int    `json:"product_quantity"`
}

type ListCategoryResponse struct {
	Name      string `json:"name"`
	Thumbnail string `json:"thumbnail"`
}

func (ca *CategoryDetailSerializer) DetailResponse() CategoryDetailResponse {
	category := CategoryDetailResponse{
		ID:              ca.Category.ID,
		Name:            ca.Category.Name,
		Thumbnail:       ca.Category.Thumbnail,
		Description:     ca.Category.Description,
		ProductQuantity: ca.Category.ProductQuantity,
	}
	return category
}

func (u *ListCategorySerializer) ListResponse() []ListCategoryResponse {
	categories := []ListCategoryResponse{}
	for i := 0; i < len(u.Category); i++ {
		categories = append(categories, ListCategoryResponse{
			Name:      u.Category[i].Name,
			Thumbnail: u.Category[i].Thumbnail,
		})
	}
	return categories
}
