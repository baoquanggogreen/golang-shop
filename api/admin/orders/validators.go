package orders

import (
	"errors"
	"strconv"
	"web-service-gin/common"
	"web-service-gin/middlewares"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

type OrderModelValidator struct {
	Name          string `json:"name" binding:"required"`
	PaymentTypeID uint   `json:"payment_type_id" binding:"required"`
	UserID        uint   `json:"user_id" binding:"required"`
	CartID        uint   `json:"cart_id" binding:"required"`
	TotalAmount   int    `json:"total_amount"`
	OrderProducts []models.OrderProduct
	orderModel    models.Order `json:"-"`
}

type ListOrderValidator struct {
}

type StatusOrderValidator struct {
	Status     string       `json:"status" binding:"required"`
	orderModel models.Order `json:"-"`
}

func (u *OrderModelValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}

	cart, _ := models.FindOneCart(&models.Cart{ID: u.CartID})
	if middlewares.GetCurrentUser(c) != cart.UserID {
		return errors.New("user doesn't own this cart")
	}

	u.orderModel.Name = u.Name
	u.orderModel.PaymentTypeID = u.PaymentTypeID
	u.orderModel.UserID = u.UserID
	u.orderModel.TotalAmount = cart.TotalAmountCart()
	return nil
}

func (u *ListOrderValidator) Bind(c *gin.Context) (common.PagingModel, error) {
	err := common.Bind(c, u)
	if err != nil {
		return common.PagingModel{}, err
	}

	page, _ := strconv.Atoi(c.Query("page"))
	pageSize, _ := strconv.Atoi(c.Query("page_size"))

	return common.PagingModel{Page: page, PageSize: pageSize}, nil
}

func (u *StatusOrderValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}

	u.orderModel.Status = u.Status
	return nil
}

func NewOrderModelValidator() OrderModelValidator {
	orderModelValidator := OrderModelValidator{}
	return orderModelValidator
}

func NewListOrderValidator() ListOrderValidator {
	listOrderValidator := ListOrderValidator{}
	return listOrderValidator
}

func NewStatusOrderValidator() StatusOrderValidator {
	statusOrderValidator := StatusOrderValidator{}
	return statusOrderValidator
}
