package models

import (
	"errors"
	"web-service-gin/common"
	"web-service-gin/config"

	"gorm.io/gorm"
)

type Category struct {
	ID                uint              `gorm:"primary_key" json:"id"`
	Name              string            `json:"name" gorm:"type:string;unique"`
	Thumbnail         string            `json:"thumbnail" gorm:"type:string"`
	Description       string            `json:"description" gorm:"type:string"`
	ProductQuantity   int               `json:"product_quantity" gorm:"type:int"`
	ProductCategories []ProductCategory `json:"product_category"`
}

func FindOneCategory(condition interface{}) (Category, error) {
	db := config.GetDB()
	var category Category
	err := db.Model(&Category{}).Where(condition).First(&category).Error
	return category, err
}

func GetListCategory(paging interface{}) []Category {
	page := paging.(*common.PagingModel).Page
	page_size := paging.(*common.PagingModel).PageSize
	db := config.GetDB()
	var categories []Category
	db.Order(
		"name, product_quantity",
	).Scopes(
		common.Paginate(page, page_size),
	).Find(
		&categories,
	)
	return categories
}

func SaveOneCategory(data interface{}) error {
	db := config.GetDB()
	err := db.Save(data).Error
	return err
}

func UpdateCategory(id uint, data interface{}) error {
	db := config.GetDB()
	err := db.Debug().Where("ID = ?", id).Updates(data).Error
	return err
}

func (c *Category) BeforeDelete(tx *gorm.DB) (err error) {
	db := config.GetDB()
	var check_exist bool
	exist := db.Debug().Model(&ProductCategory{}).Where("category_id = ?", c.ID).Find(&check_exist).Error
	if exist != nil {
		return errors.New("can't delete this category")
	}

	return err
}

func DeleteCategory(condition interface{}) error {
	db := config.GetDB()
	error := db.Transaction(func(tx *gorm.DB) error {
		_ = tx
		err := db.Delete(condition).Error
		return err
	})
	return error
}
