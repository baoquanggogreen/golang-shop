package products

import (
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

type ProductSerializer struct {
	c       *gin.Context
	Product models.Product
}

type ListProductSerializer struct {
	c       *gin.Context
	Product []models.Product
}

type ProductDetailResponse struct {
	Name            string                 `json:"name"`
	Price           int                    `json:"price"`
	TotalInStock    uint                   `json:"total_in_stock"`
	Picture         []models.Picture       `json:"pictures"`
	Description     string                 `json:"description"`
	RemainingAmount uint                   `json:"remaining_amount"`
	Category        []ProductCategoryItems `json:"categories"`
}

type ProductCategoryItems struct {
	Name            string `json:"name"`
	Thumbnail       string `json:"thumbnail"`
	Description     string `json:"description"`
	ProductQuantity int    `json:"product_quantity"`
}

type ListProductResponse struct {
	Name             string `json:"name"`
	DefaultThumbnail string `json:"default_thumbnail"`
	Price            int    `json:"price"`
}

func (u *ProductSerializer) DetailResponse() ProductDetailResponse {
	category_items := []ProductCategoryItems{}
	for i := 0; i < len(u.Product.ProductCategories); i++ {
		category_items = append(category_items, ProductCategoryItems{
			Name:            u.Product.ProductCategories[i].Category.Name,
			Thumbnail:       u.Product.ProductCategories[i].Category.Thumbnail,
			Description:     u.Product.ProductCategories[i].Category.Description,
			ProductQuantity: u.Product.ProductCategories[i].Category.ProductQuantity,
		})
	}
	product := ProductDetailResponse{
		Name:            u.Product.Name,
		Price:           u.Product.Price,
		TotalInStock:    u.Product.TotalInStock,
		Picture:         u.Product.Pictures,
		Description:     u.Product.Description,
		RemainingAmount: u.Product.RemainingAmount,
		Category:        category_items,
	}
	return product
}

func (u *ListProductSerializer) ListResponse() []ListProductResponse {
	products := []ListProductResponse{}
	for i := 0; i < len(u.Product); i++ {
		products = append(products, ListProductResponse{
			Name:             u.Product[i].Name,
			Price:            u.Product[i].Price,
			DefaultThumbnail: models.GetThumbnail(u.Product[i]).Url,
		})
	}
	return products
}
