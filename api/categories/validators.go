package categories

import (
	"strconv"
	"web-service-gin/common"
	"github.com/gin-gonic/gin"
)

type ListCategoryValidator struct {
}

func (u *ListCategoryValidator) Bind(c *gin.Context) (common.PagingModel, error) {
	err := common.Bind(c, u)
	if err != nil {
		return common.PagingModel{}, err
	}
	page, _ := strconv.Atoi(c.Query("page"))
	pageSize, _ := strconv.Atoi(c.Query("page_size"))

	return common.PagingModel{Page: page, PageSize: pageSize}, nil
}

func NewListCategoryValidator() ListCategoryValidator {
	listCategoryValidator := ListCategoryValidator{}
	return listCategoryValidator
}
