package products

import (
	"net/http"
	"strconv"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func Create(c *gin.Context) {
	productModelValidator := NewProductModelValidator()
	if err := productModelValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("validation", err))
		return
	}

	product_request := productModelValidator.productModel
	err := models.SaveOneProduct(&product_request)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	product, _ := models.FindOneProduct(&models.Product{Name: product_request.Name})
	serializer := ProductSerializer{c, product}
	c.JSON(http.StatusCreated, gin.H{"product": serializer.DetailResponse()})
}

func ListProduct(c *gin.Context) {
	listProductValidator := NewListProductValidator()
	paging, err := listProductValidator.Bind(c)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	products := models.AdminGetListProduct(&paging)
	serializer := ListProductSerializer{c, products}
	c.JSON(http.StatusOK, common.ListResponse(serializer.ListResponse(), paging))
}

func Update(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	updateProductValidator := NewUpdateValidator()
	if err := updateProductValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}

	product, err := models.FindOneProduct(&models.Product{ID: id})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}

	updated_product, err_update := product.UpdateProduct(&updateProductValidator.productModel)
	if err_update != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("validation", err_update))
		return
	}
	serializer := ProductSerializer{c, updated_product}
	c.JSON(http.StatusOK, gin.H{"products": serializer.DetailResponse()})
}

func Delete(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	_, exist := models.FindOneProduct(&models.Product{ID: id})
	if exist != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}

	err := models.DeleteProduct(&models.Product{ID: id})
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Product deleted"})
}
