package models

import (
	"errors"
	"time"
	"web-service-gin/common"
	"web-service-gin/config"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type User struct {
	ID         uint      `gorm:"primary_key" json:"id"`
	FullName   string    `json:"full_name" gorm:"type:string"`
	Username   string    `json:"user_name" gorm:"type:string;unique"`
	Avatar     string    `json:"avatar" gorm:"type:string"`
	Password   string    `json:"password" gorm:"type:string;size:1024"`
	Email      string    `json:"email" gorm:"type:string"`
	ResetToken string    `json:"reset_token" gorm:"type:string"`
	Is_active  bool      `json:"is_active" gorm:"type:boolean"`
	Is_blocked bool      `json:"is_blocked" gorm:"type:boolean"`
	Phone      string    `json:"phone" gorm:"type:string"`
	Birthday   string    `json:"birthday" gorm:"type:string"`
	Gender     string    `json:"gender" gorm:"type:string"`
	Address    string    `json:"address" gorm:"type:string"`
	CreatedAt  time.Time `json:"created_at" gorm:"type:timestamp"`
	UpdatedAt  time.Time `json:"updated_at" gorm:"type:timestamp"`
	DeletedAt  gorm.DeletedAt
	ExpiredAt  time.Time `json:"expired_at" gorm:"type:timestamp"`
	Cart       Cart      `json:"cart"`
	Orders     []Order   `json:"orders"`
}

func FindOneUser(condition interface{}) (User, error) {
	db := config.GetDB()
	var user User
	err := db.Preload("Cart").Model(&User{}).Where(condition).First(&user).Error
	return user, err
}

func SaveOneUser(data interface{}) error {
	db := config.GetDB()
	err := db.Save(data).Error
	return err
}

func (u *User) Update(data interface{}) error {
	db := config.GetDB()
	err := db.Where(u).Updates(data).Error
	return err
}

func (u *User) SetPassword(password string) error {
	if len(password) == 0 {
		return errors.New("password should not be empty")
	}
	bytePassword := []byte(password)
	passwordHash, _ := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)
	u.Password = string(passwordHash)
	return nil
}

func (u *User) CheckValidAuth(password string) bool {
	check_password := bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
	check_active_user := u.Is_active && !u.Is_blocked
	if check_password == nil && check_active_user {
		return true
	}
	return false
}

func (u *User) SaveActivateCode(activate_code string) {
	db := config.GetDB()
	u.ResetToken = activate_code
	u.ExpiredAt = time.Now().Add(time.Hour * common.FORGOT_PASSWORD_EXPIRE)
	db.Save(&u)
}

func (u *User) ActivateAccount() {
	db := config.GetDB()
	u.ResetToken = ""
	u.Is_active = true
	db.Save(&u)
}

func (u *User) SaveForgotPasswordToken(password_token string) {
	db := config.GetDB()
	expired_time := time.Now().Add(time.Hour * common.FORGOT_PASSWORD_EXPIRE)
	db.Model(&u).Where("Email = ?", u.Email).Updates(
		map[string]interface{}{
			"reset_token": password_token,
			"expired_at":  expired_time,
		},
	)
}

func (u *User) SaveNewPassword(new_password string) {
	db := config.GetDB()
	u.SetPassword(new_password)
	u.ResetToken = ""
	db.Save(&u)
}

func GetListUser(paging interface{}) []User {
	page := paging.(*common.PagingModel).Page
	page_size := paging.(*common.PagingModel).PageSize
	db := config.GetDB()
	var users []User
	db.Scopes(
		common.Paginate(page, page_size),
	).Find(&users)

	return users
}

func DeleteOneUser(condition interface{}) error {
	db := config.GetDB()
	var user User
	error := db.Transaction(func(tx *gorm.DB) error {
		_= tx
		err := db.Where(condition).Where("Is_active = ?", false).First(&user).Error
		db.Delete(&user)
		return err
	})
	return error
}

func BlockOneUser(condition interface{}) error {
	db := config.GetDB()
	var user User
	error := db.Transaction(func(tx *gorm.DB) error {
		_ = tx
		err := db.Where(condition).Where("Is_active = ?", true).First(&user).Error
		if user.Is_blocked {
			err_block := errors.New("user is already blocked")
			if err_block != nil {
				return err_block
			}
		}
		user.Is_blocked = true
		db.Save(&user)
		return err
	})
	return error
}

func UnblockOneUser(condition interface{}) error {
	db := config.GetDB()
	var user User
	error := db.Transaction(func(tx *gorm.DB) error {
		_ = tx
		err := db.Where(condition).Where("Is_active = ?", true).First(&user).Error
		if !user.Is_blocked {
			err = errors.New("user is not blocked")
		}
		user.Is_blocked = false
		db.Save(&user)
		return err
	})
	return error
}
