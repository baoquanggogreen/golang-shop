package carts

import (
	"github.com/gin-gonic/gin"
	"web-service-gin/models"
)

type CartSerializer struct {
	c    *gin.Context
	Cart models.Cart
}

type CartDetailResponse struct {
	UserID uint         `json:"user_id"`
	Items  []ItemDetail `json:"items"`
}

type ItemDetail struct {
	ProductName string `json:"product_name"`
	Thumbnail   string `json:"thumbnail"`
	Quantity    uint   `json:"quantity"`
	Price       int    `json:"price"`
}

func (u *CartSerializer) DetailResponse() CartDetailResponse {
	cart_items := []ItemDetail{}
	for i := 0; i < len(u.Cart.CartProducts); i++ {
		cart_items = append(cart_items, ItemDetail{
			ProductName: u.Cart.CartProducts[i].Product.Name,
			Thumbnail:   models.GetThumbnail(u.Cart.CartProducts[i].Product).Url,
			Quantity:    u.Cart.CartProducts[i].Quantity,
			Price:       u.Cart.CartProducts[i].Product.Price,
		})
	}
	cart := CartDetailResponse{
		UserID: u.Cart.UserID,
		Items:  cart_items,
	}
	return cart
}
