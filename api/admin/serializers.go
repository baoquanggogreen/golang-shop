package admin

import (
	"github.com/gin-gonic/gin"
	"web-service-gin/common"
	"web-service-gin/models"
)

type AdminDetailSerializer struct {
	c *gin.Context
	models.Admin
}

type AdminLoginResponse struct {
	Token string `json:"token"`
}

type AdminDetailResponse struct {
	Username string `json:"username"`
	Email    string `json:"email"`
}

func (u *AdminDetailSerializer) RegisterResponse() AdminDetailResponse {
	admin := AdminDetailResponse{
		Username: u.Username,
		Email:    u.Email,
	}
	return admin
}

func (u *AdminDetailSerializer) DetailResponse() AdminDetailResponse {
	admin := AdminDetailResponse{
		Username: u.Username,
		Email:    u.Email,
	}
	return admin
}

func (u *AdminDetailSerializer) LoginResponse() AdminLoginResponse {
	admin := AdminLoginResponse{
		Token: common.GenAuthToken(u.ID, true),
	}
	return admin
}
