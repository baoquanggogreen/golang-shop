package common

import "github.com/gin-gonic/gin"

func Unauthenticated(c *gin.Context, err error) {
	if err != nil {
		c.JSON(401, gin.H{"error": err.Error()})
		c.Abort()
		return
	}
}
