package users

import (
	"net/http"
	"time"
	"web-service-gin/common"
	"web-service-gin/middlewares"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func GetDetail(c *gin.Context) {
	user_id := middlewares.GetCurrentUser(c)
	user, err := models.FindOneUser(&models.User{ID: user_id})
	serializer := UserDetailSerializer{c, user}
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	serializer = UserDetailSerializer{c, user}
	c.JSON(http.StatusOK, gin.H{"user": serializer.DetailResponse()})
}

func Create(c *gin.Context) {
	userModelValidator := NewUserModelValidator()
	if err := userModelValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}

	user := userModelValidator.userModel
	if err := models.SaveOneUser(&user); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	activation_code := common.GenResetToken()
	user.SaveActivateCode(activation_code)
	go func() {
		_ = common.SendActivationMail(user.Email, activation_code)
	}()
	serializer := UserDetailSerializer{c, user}
	c.JSON(http.StatusCreated, gin.H{"user": serializer.RegisterResponse()})
}

func ResendActiveUser(c *gin.Context) {
	userModelValidator := NewUserActiveCodeValidator()
	if err := userModelValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}

	user, err := models.FindOneUser(&models.User{Email: userModelValidator.userModel.Email})
	_ = user
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}

	activation_code := common.GenResetToken()
	user.SaveActivateCode(activation_code)
	go func() {
		_ = common.SendActivationMail(user.Email, activation_code)
	}()
	c.JSON(http.StatusOK, gin.H{"message": "Resend success"})
}

func UpdateDetail(c *gin.Context) {
	user_id := middlewares.GetCurrentUser(c)
	user, err := models.FindOneUser(&models.User{ID: user_id})
	_ = user
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	userModelValidator := NewUserUpdateValidator()
	if err := userModelValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}

	if err := user.Update(&userModelValidator.userModel); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	serializer := UserDetailSerializer{c, user}
	c.JSON(http.StatusOK, gin.H{"user": serializer.DetailResponse()})
}

func ConfirmEmail(c *gin.Context) {
	userActiveCodeValidator := NewUserActiveCodeValidator()
	if err := userActiveCodeValidator.Bind(c); err != nil {
		c.JSON(http.StatusBadRequest, common.NewValidatorError(err))
		return
	}
	user, err := models.FindOneUser(&models.User{ResetToken: userActiveCodeValidator.userModel.ResetToken})
	if err != nil {
		c.JSON(http.StatusNotAcceptable, common.NewError("database", err))
		return
	}
	if user.ExpiredAt.Before(time.Now()) {
		c.JSON(http.StatusBadRequest, gin.H{"validation": "Token expired or not exist, please resend"})
		return
	}
	user.ActivateAccount()
	serializer := UserDetailSerializer{c, user}
	c.JSON(http.StatusOK, gin.H{"user": serializer.DetailResponse()})
}

func Login(c *gin.Context) {
	userLoginValidator := NewUserLoginValidator()
	if err := userLoginValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	user_request := userLoginValidator.userModel
	current_user, check_exist := models.FindOneUser(&models.User{Username: user_request.Username})
	check_valid := current_user.CheckValidAuth(user_request.Password)
	if !check_valid || check_exist != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"message": "Invalid Request"})
		return
	}
	serializer := UserDetailSerializer{c, current_user}
	c.JSON(http.StatusCreated, gin.H{"user": serializer.LoginResponse()})
}

func ResetPasswordEntry(c *gin.Context) {
	userPasswordEntryValidator := NewResetPasswordEntryValidator()
	if err := userPasswordEntryValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}

	user := userPasswordEntryValidator.userModel
	password_token := common.GenResetToken()
	user.SaveForgotPasswordToken(password_token)
	go func() {
		err_mail := common.SendResetPasswordMail(user.Email, password_token)
		if err_mail != nil {
			c.JSON(http.StatusInternalServerError, gin.H{"message": "Mail has not been sent"})
			return
		}
	}()
	c.JSON(http.StatusCreated, gin.H{"message": "Email sent to " + user.Email})
}

func ResetPassword(c *gin.Context) {
	userPasswordValidator := NewResetPasswordValidator()
	if err := userPasswordValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("validation", err))
		return
	}

	user_record, _ := models.FindOneUser(&models.User{ResetToken: userPasswordValidator.ResetToken})
	if user_record.ExpiredAt.Before(time.Now()) || user_record.ResetToken == "" {
		c.JSON(http.StatusBadRequest, gin.H{"validation": "Token expired or not exist, please resend"})
		return
	}
	user_record.SaveNewPassword(userPasswordValidator.NewPassword)
	c.JSON(http.StatusCreated, gin.H{"message": "Change password success"})
}

func UploadAvatar(c *gin.Context) {
	file, _ := c.FormFile("file")

	url, err := common.UploadSingle(file)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("validation", err))
		return
	}
	user, _ := models.FindOneUser(&models.User{ID: middlewares.GetCurrentUser(c)})
	user.Update(&models.User{Avatar: url})
	c.JSON(http.StatusOK, gin.H{"data": url})
}
