package common

import (
	"bytes"
	"fmt"
	"net/smtp"
	"os"
	"text/template"

	"github.com/joho/godotenv"
)

func SendActivationMail(to string, activation_code string) error {
	godotenv.Load(".env")
	from := os.Getenv("EMAIL_NAME")
	pass := os.Getenv("EMAIL_PASSWORD")
	host := "smtp.gmail.com:" + os.Getenv("EMAIL_PORT")

	t, _ := template.ParseFiles("template/active_account.html")

	var test bytes.Buffer

	mimeHeaders := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	test.Write([]byte(fmt.Sprintf("Subject: Activation Mail \n%s\n\n", mimeHeaders)))

	t.Execute(&test, struct {
		Token string
	}{
		Token: activation_code,
	})
	err := smtp.SendMail(host,
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, test.Bytes())

	if err != nil {
		return err
	}
	return err
}

func SendResetPasswordMail(to string, password_token string) error {
	godotenv.Load(".env")
	from := os.Getenv("EMAIL_NAME")
	pass := os.Getenv("EMAIL_PASSWORD")
	host := "smtp.gmail.com:" + os.Getenv("EMAIL_PORT")

	t, _ := template.ParseFiles("template/reset_password.html")

	var test bytes.Buffer

	mimeHeaders := "MIME-version: 1.0;\nContent-Type: text/html; charset=\"UTF-8\";\n\n"
	test.Write([]byte(fmt.Sprintf("Subject: Reset password \n%s\n\n", mimeHeaders)))

	t.Execute(&test, struct {
		Token string
	}{
		Token: password_token,
	})
	err := smtp.SendMail(host,
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, test.Bytes())

	if err != nil {
		return err
	}
	return err
}

func SendOrderMail(
	to string,
	subject string,
	body string,
	name string,
	payment_type string,
	status string,
	order_date string,
) error {
	godotenv.Load(".env")
	from := os.Getenv("EMAIL_NAME")
	pass := os.Getenv("EMAIL_PASSWORD")
	host := "smtp.gmail.com:" + os.Getenv("EMAIL_PORT")

	order := "Name: " + name + "\n" +
		"Payment status: " + payment_type + "\n" +
		"Status: " + status + "\n" +
		"Order date: " + order_date

	msg := "From: " + from + "\n" +
		"To: " + to + "\n" +
		"Subject: " + subject + "\n\n" +
		body + "Your order is: \n" + order

	err := smtp.SendMail(host,
		smtp.PlainAuth("", from, pass, "smtp.gmail.com"),
		from, []string{to}, []byte(msg))

	if err != nil {
		return err
	}
	return err
}
