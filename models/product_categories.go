package models

import "gorm.io/gorm"

type ProductCategory struct {
	ID         uint     `json:"id" gorm:"primary_key"`
	Category   Category `json:"category"`
	CategoryID uint     `json:"category_id" gorm:"type:int"`
	Product    Product  `json:"product"`
	ProductID  uint     `json:"product_id" gorm:"type:int"`
	DeletedAt  gorm.DeletedAt
}
