package users

import (
	"github.com/gin-gonic/gin"
	"web-service-gin/common"
	"web-service-gin/models"
)

type UserDetailSerializer struct {
	c *gin.Context
	models.User
}

type UserLoginResponse struct {
	Token string `json:"token"`
}

type UserDetailResponse struct {
	Username string `json:"username"`
	Email    string `json:"email"`
	Fullname string `json:"full_name"`
	Phone    string `json:"phone"`
	Birthday string `json:"birthday"`
	Gender   string `json:"gender"`
	Address  string `json:"address"`
}

func (u *UserDetailSerializer) RegisterResponse() UserDetailResponse {
	user := UserDetailResponse{
		Username: u.Username,
		Email:    u.Email,
		Fullname: u.FullName,
		Phone:    u.Phone,
		Birthday: u.Birthday,
		Gender:   u.Gender,
		Address:  u.Address,
	}
	return user
}

func (u *UserDetailSerializer) DetailResponse() UserDetailResponse {
	user := UserDetailResponse{
		Username: u.Username,
		Fullname: u.FullName,
		Email:    u.Email,
		Phone:    u.Phone,
		Birthday: u.Birthday,
		Gender:   u.Gender,
		Address:  u.Address,
	}
	return user
}

func (u *UserDetailSerializer) LoginResponse() UserLoginResponse {
	user := UserLoginResponse{
		Token: common.GenAuthToken(u.ID, false),
	}
	return user
}
