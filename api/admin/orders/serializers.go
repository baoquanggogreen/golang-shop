package orders

import (
	"time"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

type OrderSerializer struct {
	c     *gin.Context
	Order models.Order
}

type ListOrderSerializer struct {
	c     *gin.Context
	Order []models.Order
}

type OrderDetailResponse struct {
	ID            uint   `json:"id"`
	Name          string `json:"name"`
	PaymentType   string `json:"payment_type"`
	Status        string `json:"status"`
	PaymentStatus string `json:"payment_status"`
	OrderDate     string `json:"order_date"`
	PaymentDate   string `json:"payment_date"`
	CompletedDate string `json:"completed_date"`
	TotalAmount   int    `json:"total_amount"`
}

type ListOrderResponse struct {
	Name          string         `json:"name"`
	CompletedDate string         `json:"completed_date"`
	ItemOrder     models.Product `json:"order_item"`
	TotalItem     int            `json:"total_item"`
	TotalAmount   int            `json:"total_amount"`
}

type OrderDetailsResponse struct {
	OrderDate     string            `json:"order_date"`
	PaymentDate   string            `json:"payment_date"`
	CompletedDate string            `json:"completed_date"`
	ItemOrder     []OrderItemDetail `json:"item_orders"`
	TotalAmount   int               `json:"total_amount"`
}

type OrderItemDetail struct {
	ProductName string `json:"product_name"`
	Thumbnail   string `json:"thumbnail"`
	Quantity    uint   `json:"quantity"`
	Price       int    `json:"price"`
}

func (u *OrderSerializer) DetailResponse() OrderDetailResponse {
	paymentDate := ""
	completedDate := ""
	if u.Order.PaymentDate.Equal(time.Time{}) {
		paymentDate = u.Order.PaymentDate.Format(common.FORMAT_DATE_TIME)
	}
	if u.Order.CompletedDate.Equal(time.Time{}) {
		completedDate = u.Order.CompletedDate.Format(common.FORMAT_DATE_TIME)
	}
	order := OrderDetailResponse{
		Name:          u.Order.Name,
		PaymentType:   u.Order.PaymentType.Type,
		Status:        u.Order.Status,
		PaymentStatus: u.Order.PaymentStatus,
		OrderDate:     u.Order.OrderDate.Format(common.FORMAT_DATE_TIME),
		PaymentDate:   paymentDate,
		CompletedDate: completedDate,
		TotalAmount:   u.Order.TotalAmount,
	}
	return order
}

func (u *ListOrderSerializer) ListResponse() []ListOrderResponse {
	completedDate := ""
	orders := []ListOrderResponse{}
	for i := 0; i < len(u.Order); i++ {
		if u.Order[i].CompletedDate.Equal(time.Time{}) {
			completedDate = u.Order[i].CompletedDate.Format(common.FORMAT_DATE_TIME)
		}
		totalItem := len(u.Order[i].OrderProducts)
		orders = append(orders, ListOrderResponse{
			Name:          u.Order[i].Name,
			CompletedDate: completedDate,
			ItemOrder:     u.Order[0].OrderProducts[0].Product,
			TotalItem:     totalItem,
			TotalAmount:   u.Order[i].TotalAmount,
		})
	}
	return orders
}

func (u *OrderSerializer) DetailsResponse() OrderDetailsResponse {
	paymentDate := ""
	completedDate := ""
	if u.Order.PaymentDate.Equal(time.Time{}) {
		paymentDate = u.Order.PaymentDate.Format(common.FORMAT_DATE_TIME)
	}
	if u.Order.CompletedDate.Equal(time.Time{}) {
		completedDate = u.Order.CompletedDate.Format(common.FORMAT_DATE_TIME)
	}
	order_items := []OrderItemDetail{}
	for i := 0; i < len(u.Order.OrderProducts); i++ {
		order_items = append(order_items, OrderItemDetail{
			ProductName: u.Order.OrderProducts[i].Product.Name,
			Thumbnail:   models.GetThumbnail(u.Order.OrderProducts[i].Product).Url,
			Quantity:    u.Order.OrderProducts[i].Quantity,
			Price:       u.Order.OrderProducts[i].Product.Price,
		})
	}

	order := OrderDetailsResponse{
		OrderDate:     u.Order.OrderDate.Format(common.FORMAT_DATE_TIME),
		PaymentDate:   paymentDate,
		CompletedDate: completedDate,
		TotalAmount:   u.Order.TotalAmount,
		ItemOrder:     order_items,
	}
	return order
}
