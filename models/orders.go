package models

import (
	"time"
	"web-service-gin/common"
	"web-service-gin/config"

	"gorm.io/gorm"
)

type Order struct {
	ID            uint           `gorm:"primary_key" json:"id"`
	Name          string         `json:"name" gorm:"type:string;unique"`
	PaymentType   Payment        `json:"payment_type"`
	PaymentTypeID uint           `json:"payment_type_id" gorm:"type:int"`
	UserID        uint           `json:"user_id" gorm:"type:int"`
	Status        string         `json:"status" gorm:"type:string"`
	PaymentStatus string         `json:"payment_status" gorm:"type:string"`
	OrderDate     time.Time      `json:"order_date" gorm:"type:timestamp"`
	PaymentDate   time.Time      `json:"payment_date" gorm:"type:timestamp"`
	CompletedDate time.Time      `json:"completed_date" gorm:"type:timestamp"`
	TotalAmount   int            `json:"total_amount" gorm:"type:int"`
	OrderProducts []OrderProduct `json:"order_item"`
}

type Payment struct {
	ID   uint   `gorm:"primary_key" json:"id"`
	Type string `json:"type" gorm:"type:string"`
}

type OrderProduct struct {
	ID        uint    `gorm:"primary_key" json:"id"`
	Order     Order   `json:"order"`
	OrderID   uint    `json:"order_id" gorm:"type:int"`
	Product   Product `json:"product"`
	ProductID uint    `json:"product_id" gorm:"type:int"`
	Quantity  uint    `json:"quantity" gorm:"type:int"`
	DeletedAt gorm.DeletedAt
}

func SaveOneOrder(data interface{}) (Order, error) {
	db := config.GetDB()
	var order Order
	error := db.Transaction(func(tx *gorm.DB) error {
		_ = tx
		order_request := data.(*Order)
		paymentStatus := ""
		orderDate := time.Now()
		status := "PENDING"
		paymentDate := time.Time{}
		user, _ := FindOneUser(&User{ID: order_request.UserID})
		cart, _ := FindOneCart(&Cart{ID: user.Cart.ID})

		switch order_request.PaymentTypeID {
		case 1:
			paymentStatus = "PENDING"
		case 2:
			paymentStatus = "COMPLETED"
			paymentDate = time.Now()
		}
		order_data := Order{
			Name:          order_request.Name,
			PaymentTypeID: order_request.PaymentTypeID,
			UserID:        order_request.UserID,
			Status:        status,
			PaymentStatus: paymentStatus,
			OrderDate:     orderDate,
			PaymentDate:   paymentDate,
			TotalAmount:   cart.TotalAmountCart(),
		}
		err := db.Save(&order_data).Error
		order = order_data
		order_products := []OrderProduct{}
		cart_products := cart.CartProducts
		for i := 0; i < len(cart_products); i++ {
			order_products = append(
				order_products,
				OrderProduct{
					OrderID:   order_data.ID,
					ProductID: cart_products[i].ProductID,
					Quantity:  cart_products[i].Quantity,
				},
			)
		}
		db.CreateInBatches(order_products, len(order_products))
		db.Where("ID = ?", user.Cart.ID).Association("CartProducts").Delete(&CartProduct{})
		return err
	})
	return order, error
}

func GetListOrder(condition interface{}, paging interface{}) []Order {
	page := paging.(*common.PagingModel).Page
	page_size := paging.(*common.PagingModel).PageSize
	user_id := condition.(*Order).UserID

	var orders []Order
	db := config.GetDB()
	db.Debug().Preload(
		"OrderProducts.Product",
	).Order(
		"order_date, total_amount",
	).Scopes(
		common.Paginate(page, page_size),
	).Where(
		"user_id = ?", user_id,
	).Find(&orders)

	return orders
}

func AdminGetListOrder(paging interface{}) []Order {
	page := paging.(*common.PagingModel).Page
	page_size := paging.(*common.PagingModel).PageSize
	db := config.GetDB()
	var orders []Order
	db.Debug().Preload(
		"OrderProducts.Product",
	).Scopes(
		common.Paginate(page, page_size),
	).Find(&orders)

	return orders
}

func FindOneOrder(condition interface{}) (Order, error) {
	db := config.GetDB()
	var order Order
	err := db.Model(&Order{}).Preload("OrderProducts.Product").Where(condition).First(&order).Error
	return order, err
}

func (o *Order) ChangeStatus(condition interface{}) {
	db := config.GetDB()
	var paymentDate time.Time
	var completedDate time.Time
	if o.PaymentTypeID == 1 {
		paymentDate = time.Now()
		completedDate = time.Now()
	} else {
		paymentDate = o.OrderDate
	}
	db.Model(&o).Updates(
		map[string]interface{}{
			"Status":        condition.(*Order).Status,
			"PaymentDate":   paymentDate,
			"CompletedDate": completedDate,
		},
	)
}
