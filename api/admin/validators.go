package admin

import (
	"os"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

type AdminModelValidator struct {
	Username   string       `json:"username" binding:"required"`
	Email      string       `json:"email" binding:"required,email"`
	Password   string       `json:"password" binding:"required"`
	adminModel models.Admin `json:"-"`
}

type AdminLoginValidator struct {
	Username   string       `json:"username" binding:"required"`
	Password   string       `json:"password" binding:"required"`
	adminModel models.Admin `json:"-"`
}

func (u *AdminModelValidator) Bind(c *gin.Context) error {
	godotenv.Load(".env")
	err := common.Bind(c, u)
	if err != nil {
		return err
	}

	u.adminModel.Username = u.Username
	u.adminModel.Email = u.Email

	if u.Password != os.Getenv("RANDOM_PASSWORD") {
		u.adminModel.SetPassword(u.Password)
	}
	return nil
}

func (u *AdminLoginValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}
	u.adminModel.Username = u.Username
	u.adminModel.Password = u.Password
	return nil
}

// put default here
func NewAdminModelValidator() AdminModelValidator {
	adminModelValidator := AdminModelValidator{}
	return adminModelValidator
}

func NewAdminLoginValidator() AdminLoginValidator {
	adminLoginValidator := AdminLoginValidator{}
	return adminLoginValidator
}
