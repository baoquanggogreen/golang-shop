package middlewares

import (
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func UserAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.GetHeader("Authorization")
		if tokenString == "" {
			c.JSON(401, gin.H{"error": "Unauthorized"})
			c.Abort()
			return
		}
		user_id, is_admin, err := common.ValidateToken(tokenString)
		common.Unauthenticated(c, err)

		if is_admin {
			c.JSON(401, gin.H{"error": "Not login with user role"})
			c.Abort()
			return
		}
		user, err := models.FindOneUser(&models.User{ID: user_id})
		common.Unauthenticated(c, err)
		if !user.Is_active || user.Is_blocked {
			c.JSON(401, gin.H{"error": "Unactive or blocked user"})
			c.Abort()
			return
		}
		c.Next()
	}
}

func AdminAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		tokenString := c.GetHeader("Authorization")
		if tokenString == "" {
			c.JSON(401, gin.H{"error": "Unauthorized"})
			c.Abort()
			return
		}
		user_id, is_admin, err := common.ValidateToken(tokenString)
		common.Unauthenticated(c, err)

		if !is_admin {
			c.JSON(401, gin.H{"error": "Not login with admin role"})
			c.Abort()
			return
		}
		_, error := models.FindOneAdmin(&models.Admin{ID: user_id})
		common.Unauthenticated(c, error)
		c.Next()
	}
}

func GetCurrentUser(c *gin.Context) (user_id uint) {
	tokenString := c.GetHeader("Authorization")
	user_id, is_admin, err := common.ValidateToken(tokenString)
	if err != nil || is_admin {
		panic(err.Error())
	}
	return user_id
}
