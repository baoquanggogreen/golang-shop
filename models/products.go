package models

import (
	"errors"
	"web-service-gin/common"
	"web-service-gin/config"

	"gorm.io/gorm"
	"gorm.io/gorm/clause"
)

type Product struct {
	ID                uint              `gorm:"primary_key" json:"id"`
	Name              string            `json:"name" gorm:"type:string;unique"`
	Price             int               `json:"price" gorm:"type:int"`
	TotalInStock      uint              `json:"total_in_stock" gorm:"type:int"`
	SoldAmount        uint              `json:"sold_amount" gorm:"type:int"`
	RemainingAmount   uint              `json:"remaining_amount" gorm:"type:int"`
	ProductCategories []ProductCategory `json:"categories"`
	Pictures          []Picture         `json:"pictures"`
	Description       string            `json:"description" gorm:"type:string"`
	DeletedAt         gorm.DeletedAt
}

type Picture struct {
	ID          uint   `json:"id" gorm:"primary_key"`
	ProductID   uint   `json:"product_id" gorm:"type:int"`
	Url         string `json:"url" gorm:"type:string"`
	IsThumbnail bool   `json:"is_thumbnail" gorm:"type:bool"`
}

type ProductRequest struct {
	Name         string           `json:"name"`
	Price        int              `json:"price"`
	TotalInStock uint             `json:"total_in_stock"`
	Description  string           `json:"description"`
	Categories   []uint           `json:"categories"`
	Pictures     []PictureRequest `json:"pictures"`
}

type PictureRequest struct {
	Url         string `json:"url"`
	IsThumbnail bool   `json:"is_thumbnail"`
}

func GetThumbnail(product Product) Picture {
	db := config.GetDB()
	var thumbnail Picture
	db.Model(&Picture{}).Where("product_id = ? AND is_thumbnail = True", product.ID).First(&thumbnail)
	return thumbnail
}

func FindOneProduct(condition interface{}) (Product, error) {
	db := config.GetDB()
	var product Product
	err := db.Preload(
		"ProductCategories.Category",
	).Preload(
		"Pictures",
	).Where(
		condition,
	).First(&product).Error
	return product, err
}

func UserGetListProduct(condition interface{}, paging interface{}) []Product {
	page := paging.(*common.PagingModel).Page
	page_size := paging.(*common.PagingModel).PageSize
	category_name := condition.(*Product).Name
	db := config.GetDB()
	var product_ids []uint
	var category_ids []uint
	var products []Product

	db.Model(&Category{}).Select("ID").Where("Name LIKE ?", "%"+category_name+"%").Find(&category_ids)
	db.Model(&ProductCategory{}).Select("ProductID").Where("category_id IN (?)", category_ids).Find(&product_ids)
	db.Debug().Where(
		"ID IN (?) ", product_ids,
	).Order(
		"name, price",
	).Scopes(
		common.Paginate(page, page_size),
	).Find(&products)

	return products
}

func AdminGetListProduct(paging interface{}) []Product {
	page := paging.(*common.PagingModel).Page
	page_size := paging.(*common.PagingModel).PageSize
	db := config.GetDB()
	var products []Product

	db.Order(
		"name, price",
	).Scopes(
		common.Paginate(page, page_size),
	).Find(&products)

	return products
}

func SaveOneProduct(data interface{}) error {
	db := config.GetDB()
	error := db.Transaction(func(tx *gorm.DB) error {
		_ = tx
		product_request := data.(*ProductRequest)
		product := &Product{
			Name:            product_request.Name,
			Price:           product_request.Price,
			TotalInStock:    product_request.TotalInStock,
			Description:     product_request.Description,
			RemainingAmount: product_request.TotalInStock,
		}
		db.Create(&product)
		// product_category
		product_categories := []ProductCategory{}
		for i := 0; i < len(product_request.Categories); i++ {
			product_categories = append(
				product_categories,
				ProductCategory{
					ProductID:  product.ID,
					CategoryID: product_request.Categories[i],
				},
			)
		}
		err := db.CreateInBatches(product_categories, len(product_categories)).Error
		// pictures
		pictures := []Picture{}
		valid_thumbnail := 0
		for i := 0; i < len(product_request.Pictures); i++ {
			is_thumbnail := product_request.Pictures[i].IsThumbnail
			if is_thumbnail {
				valid_thumbnail += 1
				if valid_thumbnail > 1 {
					return errors.New("more than 2 thumbnails per product")
				}
			}
			pictures = append(
				pictures,
				Picture{
					ProductID:   product.ID,
					Url:         product_request.Pictures[i].Url,
					IsThumbnail: is_thumbnail,
				},
			)
		}
		err_pic := db.CreateInBatches(pictures, len(pictures)).Error
		if err != nil && err_pic != nil {
			return err_pic
		}
		return err
	})
	return error
}

func DeleteProduct(condition interface{}) error {
	db := config.GetDB()
	error := db.Transaction(func(tx *gorm.DB) error {
		_ = tx
		err := db.Select(clause.Associations).Delete(condition).Error
		return err
	})
	return error
}

func (p *Product) UpdateProduct(data interface{}) (Product, error) {
	db := config.GetDB()
	product_request := data.(*ProductRequest)
	error := db.Transaction(func(tx *gorm.DB) error {
		_ = tx
		db.Model(
			&p,
		).Where(
			"ID = ?", p.ID,
		).Updates(
			map[string]interface{}{
				"Name":            product_request.Name,
				"Price":           product_request.Price,
				"TotalInStock":    product_request.TotalInStock,
				"Description":     product_request.Description,
				"RemainingAmount": product_request.TotalInStock - p.SoldAmount,
			},
		)
		pictures := []Picture{}
		valid_thumbnail := 0
		for i := 0; i < len(product_request.Pictures); i++ {
			is_thumbnail := product_request.Pictures[i].IsThumbnail
			if is_thumbnail {
				valid_thumbnail += 1
				if valid_thumbnail > 1 {
					return errors.New("more than 2 thumbnails per product")
				}
			}
			pictures = append(
				pictures,
				Picture{
					ProductID:   p.ID,
					Url:         product_request.Pictures[i].Url,
					IsThumbnail: is_thumbnail,
				},
			)
		}
		if len(pictures) > 0 {
			db.Where("product_id = ?", p.ID).Delete(&Picture{})
			err := db.CreateInBatches(pictures, len(pictures)).Error
			return err
		}
		return nil
	})
	prod, _ := FindOneProduct(&Product{ID: p.ID})
	return prod, error
}

// Hook
func (p *Product) BeforeDelete(tx *gorm.DB) (err error) {
	db := config.GetDB()
	var check_exist bool
	exist := db.Debug().Model(&CartProduct{}).Where("product_id = ?", p.ID).Find(&check_exist).Error
	if exist != nil {
		return errors.New("can't delete this product")
	}

	return err
}
