package admin

import (
	"strconv"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

type ListCategoryValidator struct {
}

type CategoryValidator struct {
	Name          string          `json:"name" binding:"required"`
	Description   string          `json:"description" binding:"required"`
	categoryModel models.Category `json:"-"`
}

func (u *ListCategoryValidator) Bind(c *gin.Context) (common.PagingModel, error) {
	err := common.Bind(c, u)
	if err != nil {
		return common.PagingModel{}, err
	}
	page, _ := strconv.Atoi(c.Query("page"))
	pageSize, _ := strconv.Atoi(c.Query("page_size"))

	return common.PagingModel{Page: page, PageSize: pageSize}, nil
}

func (u *CategoryValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}
	u.categoryModel.Name = u.Name
	u.categoryModel.Description = u.Description

	return err
}

func NewListCategoryValidator() ListCategoryValidator {
	listCategoryValidator := ListCategoryValidator{}
	return listCategoryValidator
}

func NewCategoryValidator() CategoryValidator {
	categoryValidator := CategoryValidator{}
	return categoryValidator
}
