package carts

import (
	"net/http"
	"web-service-gin/common"
	"web-service-gin/middlewares"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func Create(c *gin.Context) {
	cart := models.Cart{UserID: middlewares.GetCurrentUser(c)}
	if err := models.SaveOneCart(&cart); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	c.JSON(http.StatusCreated, gin.H{"message": "Cart created"})
}

func AddItemToCart(c *gin.Context) {
	cart, _ := models.FindOneCart(&models.Cart{UserID: middlewares.GetCurrentUser(c)})
	addItemValidator := NewAddItemModelValidator()
	if err := addItemValidator.Bind(c); err != nil {
		c.JSON(http.StatusBadRequest, common.NewError("database", err))
		return
	}
	item_request := addItemValidator.cartProductModel
	if err := cart.AddItemToCart(&item_request); err != nil {
		c.JSON(http.StatusBadRequest, common.NewError("database", err))
		return
	}
	c.JSON(http.StatusOK, gin.H{"cart": "Add success"})
}

func GetDetail(c *gin.Context) {
	cart := models.Cart{UserID: middlewares.GetCurrentUser(c)}
	cart, err := models.FindOneCart(&cart)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	serializer := CartSerializer{c, cart}
	c.JSON(http.StatusCreated, gin.H{"cart": serializer.DetailResponse()})
}

func RemoveItemFromCart(c *gin.Context) {
	cart, _ := models.FindOneCart(&models.Cart{UserID: middlewares.GetCurrentUser(c)})
	removeItemValidator := NewRemoveItemModelValidator()
	if err := removeItemValidator.Bind(c); err != nil {
		c.JSON(http.StatusBadRequest, common.NewError("database", err))
		return
	}
	item_request := removeItemValidator.cartProductModel
	if err := cart.RemoveItemFromCart(&item_request); err != nil {
		c.JSON(http.StatusBadRequest, common.NewError("database", err))
		return
	}
	c.JSON(http.StatusOK, gin.H{"cart": "Remove success"})
}
