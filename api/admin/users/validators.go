package admin

import (
	"strconv"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

type ListUserValidator struct {
	userModel models.User `json:"-"`
}

func (u *ListUserValidator) Bind(c *gin.Context) (common.PagingModel, error) {
	err := common.Bind(c, u)
	if err != nil {
		return common.PagingModel{}, err
	}
	page, _ := strconv.Atoi(c.Query("page"))
	pageSize, _ := strconv.Atoi(c.Query("page_size"))

	return common.PagingModel{Page: page, PageSize: pageSize}, nil
}

func NewListUserValidator() ListUserValidator {
	listUserValidator := ListUserValidator{}
	return listUserValidator
}
