package categories

import (
	"net/http"
	"strconv"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func GetDetail(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	category, err := models.FindOneCategory(&models.Category{ID: id})
	serializer := CategoryDetailSerializer{c, category}
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	serializer = CategoryDetailSerializer{c, category}
	c.JSON(http.StatusOK, gin.H{"category": serializer.DetailResponse()})
}

func ListCategory(c *gin.Context) {
	listCategoryValidator := NewListCategoryValidator()
	paging, err := listCategoryValidator.Bind(c)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	categories := models.GetListCategory(&paging)
	serializer := ListCategorySerializer{c, categories}
	c.JSON(http.StatusOK, common.ListResponse(serializer.ListResponse(), paging))
}
