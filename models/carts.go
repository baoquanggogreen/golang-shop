package models

import (
	"errors"
	"web-service-gin/config"

	"gorm.io/gorm"
)

type Cart struct {
	ID           uint          `gorm:"primary_key" json:"id"`
	UserID       uint          `json:"user_id" gorm:"type:int;unique"`
	CartProducts []CartProduct `json:"cart_product"`
}

type CartProduct struct {
	ID        uint    `json:"id" gorm:"primary_key"`
	Cart      Cart    `json:"cart"`
	CartID    uint    `json:"cart_id" gorm:"type:int"`
	Product   Product `json:"product"`
	ProductID uint    `json:"product_id" gorm:"type:int"`
	Quantity  uint    `json:"quantity" gorm:"type:int"`
	DeletedAt gorm.DeletedAt
}

type AddItemRequest struct {
	CartID    uint `json:"cart_id"`
	ProductID uint `json:"product_id"`
	Quantity  uint `json:"quantity"`
}

func SaveOneCart(data interface{}) error {
	db := config.GetDB()
	err := db.Save(data).Error
	return err
}

func FindOneCart(condition interface{}) (Cart, error) {
	db := config.GetDB()
	var cart Cart
	err := db.Model(&Cart{}).Preload("CartProducts.Product").Where(condition).First(&cart).Error
	return cart, err
}

func (c *Cart) AddItemToCart(data interface{}) error {
	db := config.GetDB()
	error := db.Transaction(func(tx *gorm.DB) error {
		_ = tx
		product_id := data.(*CartProduct).ProductID
		quantity := data.(*CartProduct).Quantity

		product, err_prod := FindOneProduct(product_id)
		if err_prod != nil {
			return err_prod
		}
		if product.RemainingAmount < quantity {
			return errors.New("insufficient stock quantity")
		}
		// check unique product item
		var cart_product CartProduct
		err_uniq := db.Where(CartProduct{CartID: c.ID, ProductID: product_id}).First(&cart_product).Error
		if err_uniq == nil {
			return errors.New("product in this cart exists")
		}

		item := CartProduct{CartID: c.ID, ProductID: product_id, Quantity: quantity}
		err := db.Create(&item).Error
		// cal remaining amount
		db.Model(
			&Product{},
		).Where(
			"ID = ?", product_id,
		).Updates(
			map[string]interface{}{
				"SoldAmount":      product.SoldAmount + quantity,
				"RemainingAmount": product.RemainingAmount - quantity,
			},
		)
		return err
	})
	return error
}

func (c *Cart) RemoveItemFromCart(condition interface{}) error {
	db := config.GetDB()
	error := db.Transaction(func(tx *gorm.DB) error {
		_ = tx
		product, _ := FindOneProduct(condition.(*CartProduct).ProductID)
		var cart_product CartProduct
		err := db.Where(condition).Where("cart_id = ?", c.ID).First(&cart_product).Error
		if err != nil {
			return err
		}
		db.Debug().Model(
			&product,
		).Where(
			"ID = ?", condition.(*CartProduct).ProductID,
		).Updates(
			map[string]interface{}{
				"RemainingAmount": product.RemainingAmount + uint(cart_product.Quantity),
				"SoldAmount":      product.SoldAmount - uint(cart_product.Quantity),
			},
		)
		db.Unscoped().Delete(&cart_product)
		return nil
	})
	return error
}

func (c *Cart) TotalAmountCart() int {
	db := config.GetDB()
	var sum int
	db.Model(&CartProduct{}).Where("cart_id = ?", c.ID).Select("sum(quantity)").Row().Scan(&sum)
	return sum
}
