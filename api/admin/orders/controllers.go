package orders

import (
	"net/http"
	"strconv"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func ListOrder(c *gin.Context) {
	listOrderValidator := NewListOrderValidator()
	paging, err := listOrderValidator.Bind(c)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	orders := models.AdminGetListOrder(&paging)
	serializer := ListOrderSerializer{c, orders}
	c.JSON(http.StatusOK, common.ListResponse(serializer.ListResponse(), paging))
}

func GetDetail(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	order, err := models.FindOneOrder(&models.Order{ID: id})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	serializer := OrderSerializer{c, order}
	c.JSON(http.StatusOK, gin.H{"order": serializer.DetailsResponse()})
}

func ChangeStatus(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	statusOrderValidator := NewStatusOrderValidator()
	if err := statusOrderValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("validation", err))
		return
	}

	order_request := statusOrderValidator.orderModel
	order, _ := models.FindOneOrder(&models.Order{ID: id})
	order.ChangeStatus(&order_request)
	c.JSON(http.StatusOK, gin.H{"message": "Update success"})
}
