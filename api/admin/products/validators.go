package products

import (
	"strconv"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

type ProductModelValidator struct {
	Name         string                  `json:"name" binding:"required"`
	Price        int                     `json:"price" binding:"required"`
	TotalInStock uint                    `json:"total_in_stock" binding:"required"`
	Categories   []uint                  `json:"categories" binding:"required"`
	Description  string                  `json:"description" binding:"required"`
	Pictures     []models.PictureRequest `json:"pictures" binding:"required"`
	productModel models.ProductRequest   `json:"-"`
}

type UpdateProductValidator struct {
	Name         string                  `json:"name" binding:"required"`
	Price        int                     `json:"price" binding:"required"`
	TotalInStock uint                    `json:"total_in_stock" binding:"required"`
	Description  string                  `json:"description" binding:"required"`
	Pictures     []models.PictureRequest `json:"pictures" binding:"required"`
	productModel models.ProductRequest   `json:"-"`
}

type ListProductValidator struct {
	productModel models.Product `json:"-"`
}

func (u *ProductModelValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}

	categories := []uint{}
	categories = append(categories, u.Categories...)

	u.productModel.Name = u.Name
	u.productModel.Price = u.Price
	u.productModel.TotalInStock = u.TotalInStock
	u.productModel.Categories = categories
	u.productModel.Description = u.Description
	u.productModel.Pictures = u.Pictures

	return nil
}

func (u *UpdateProductValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}

	u.productModel.Name = u.Name
	u.productModel.Price = u.Price
	u.productModel.TotalInStock = u.TotalInStock
	u.productModel.Description = u.Description
	u.productModel.Pictures = u.Pictures

	return nil
}

func (u *ListProductValidator) Bind(c *gin.Context) (common.PagingModel, error) {
	err := common.Bind(c, u)
	// TODO
	if err != nil {
		return common.PagingModel{}, err
	}

	page, _ := strconv.Atoi(c.Query("page"))
	pageSize, _ := strconv.Atoi(c.Query("page_size"))

	return common.PagingModel{Page: page, PageSize: pageSize}, nil
}

func NewProductModelValidator() ProductModelValidator {
	productModelValidator := ProductModelValidator{}
	return productModelValidator
}

func NewListProductValidator() ListProductValidator {
	listProductValidator := ListProductValidator{}
	return listProductValidator
}

func NewUpdateValidator() UpdateProductValidator {
	updateValidator := UpdateProductValidator{}
	return updateValidator
}
