package products

import (
	"net/http"
	"strconv"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func ListProduct(c *gin.Context) {
	listProductValidator := NewListProductValidator()
	paging, err := listProductValidator.Bind(c)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	product_request := listProductValidator.productModel
	products := models.UserGetListProduct(&product_request, &paging)
	serializer := ListProductSerializer{c, products}
	c.JSON(http.StatusOK, common.ListResponse(serializer.ListResponse(), paging))
}

func GetDetail(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	product, err := models.FindOneProduct(&models.Product{ID: id})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	serializer := ProductSerializer{c, product}
	c.JSON(http.StatusOK, gin.H{"order": serializer.DetailResponse()})
}
