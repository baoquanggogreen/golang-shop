package models

import (
	"errors"
	"time"
	"web-service-gin/config"

	"golang.org/x/crypto/bcrypt"
	"gorm.io/gorm"
)

type Admin struct {
	ID        uint      `gorm:"primary_key" json:"id"`
	Username  string    `json:"user_name" gorm:"type:string;unique"`
	Password  string    `json:"password" gorm:"type:string;size:1024"`
	Email     string    `json:"email" gorm:"type:string"`
	CreatedAt time.Time `json:"created_at" gorm:"type:timestamp"`
	UpdatedAt time.Time `json:"updated_at" gorm:"type:timestamp"`
	DeletedAt gorm.DeletedAt
}

func FindOneAdmin(condition interface{}) (Admin, error) {
	db := config.GetDB()
	var admin Admin
	err := db.Where(condition).First(&admin).Error
	return admin, err
}

func (a *Admin) SetPassword(password string) error {
	if len(password) == 0 {
		return errors.New("password should not be empty")
	}
	bytePassword := []byte(password)
	passwordHash, _ := bcrypt.GenerateFromPassword(bytePassword, bcrypt.DefaultCost)
	a.Password = string(passwordHash)
	return nil
}

func SaveOneAdmin(data interface{}) error {
	db := config.GetDB()
	err := db.Save(data).Error
	return err
}

func (u *Admin) CheckValidAuth(password string) error {
	return bcrypt.CompareHashAndPassword([]byte(u.Password), []byte(password))
}
