package carts

import (
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

type CartModelValidator struct {
	UserID uint `json:"user_id" binding:"required"`
}

type AddItemModelValidator struct {
	ProductID        uint               `json:"product_id" binding:"required"`
	Quantity         uint               `json:"quantity" binding:"required"`
	cartProductModel models.CartProduct `json:"-"`
}

type RemoveItemModelValidator struct {
	ProductID        uint               `json:"product_id" binding:"required"`
	cartProductModel models.CartProduct `json:"-"`
}

func (u *AddItemModelValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}

	u.cartProductModel.ProductID = u.ProductID
	u.cartProductModel.Quantity = u.Quantity

	return nil
}

func (u *RemoveItemModelValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}

	u.cartProductModel.ProductID = u.ProductID

	return nil
}

func NewAddItemModelValidator() AddItemModelValidator {
	addItemModelValidator := AddItemModelValidator{}
	return addItemModelValidator
}

func NewRemoveItemModelValidator() RemoveItemModelValidator {
	removeItemModelValidator := RemoveItemModelValidator{}
	return removeItemModelValidator
}
