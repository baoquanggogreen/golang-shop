package orders

import (
	"strconv"
	"web-service-gin/common"
	"web-service-gin/middlewares"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

type OrderModelValidator struct {
	Name          string `json:"name"`
	PaymentTypeID uint   `json:"payment_type_id"`
	UserID        uint   `json:"user_id"`
	orderModel    models.Order `json:"-"`
}

type ListOrderValidator struct {
	UserID     string       `json:"user_id"`
	orderModel models.Order `json:"-"`
}

func (u *OrderModelValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}
	current_user := middlewares.GetCurrentUser(c)

	u.orderModel.Name = u.Name
	u.orderModel.PaymentTypeID = u.PaymentTypeID
	u.orderModel.UserID = current_user
	return nil
}

func (u *ListOrderValidator) Bind(c *gin.Context) (common.PagingModel, error) {
	err := common.Bind(c, u)
	if err != nil {
		return common.PagingModel{}, err
	}

	u.orderModel.UserID = middlewares.GetCurrentUser(c)
	page, _ := strconv.Atoi(c.Query("page"))
	pageSize, _ := strconv.Atoi(c.Query("page_size"))

	return common.PagingModel{Page: page, PageSize: pageSize}, nil
}

func NewOrderModelValidator() OrderModelValidator {
	orderModelValidator := OrderModelValidator{}
	return orderModelValidator
}

func NewListOrderValidator() ListOrderValidator {
	listOrderValidator := ListOrderValidator{}
	return listOrderValidator
}
