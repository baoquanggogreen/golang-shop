package routers

import (
	admin "web-service-gin/api/admin"
	adminCategory "web-service-gin/api/admin/categories"
	adminOrder "web-service-gin/api/admin/orders"
	adminProduct "web-service-gin/api/admin/products"
	adminUser "web-service-gin/api/admin/users"
	"web-service-gin/api/carts"
	"web-service-gin/api/categories"
	"web-service-gin/api/orders"
	"web-service-gin/api/products"
	"web-service-gin/api/users"
	"web-service-gin/middlewares"

	"github.com/gin-gonic/gin"
)

func InitRouter() *gin.Engine {
	routes := gin.Default()
	v1 := routes.Group("/api/v1/")
	UserRegister(v1.Group("users/"))
	CategoryRegister(v1.Group("categories/"))
	OrderRegister(v1.Group("orders/"))
	CartRegister(v1.Group("carts/"))
	ProductRegister(v1.Group("products/"))
	AdminRegister(v1.Group("admin/"))
	return routes
}

func UserRegister(router *gin.RouterGroup) {
	router.POST("", users.Create)
	router.POST("confirm/", users.ConfirmEmail)
	router.POST("confirm-resend/", users.ResendActiveUser)
	router.POST("login/", users.Login)
	router.POST("reset-password-entry/", users.ResetPasswordEntry)
	router.POST("reset-password/", users.ResetPassword)

	authGroup := router.Group("").Use(middlewares.UserAuth())
	{
		authGroup.GET("", users.GetDetail)
		authGroup.PUT("", users.UpdateDetail)
		authGroup.POST("upload-avatar/", users.UploadAvatar)
	}
}

func OrderRegister(router *gin.RouterGroup) {
	authGroup := router.Group("").Use(middlewares.UserAuth())
	{
		authGroup.POST("", orders.Create)
		authGroup.GET("", orders.ListOrder)
		authGroup.GET(":id/", orders.GetDetail)
	}
}

func CartRegister(router *gin.RouterGroup) {
	authGroup := router.Group("").Use(middlewares.UserAuth())
	{
		authGroup.POST("", carts.Create)
		authGroup.POST("add-item/", carts.AddItemToCart)
		authGroup.GET("", carts.GetDetail)
		authGroup.POST("delete-item/", carts.RemoveItemFromCart)
	}
}

func CategoryRegister(router *gin.RouterGroup) {
	router.GET(":id/", categories.GetDetail)
	router.GET("", categories.ListCategory)
}

func ProductRegister(router *gin.RouterGroup) {
	router.GET(":id/", products.GetDetail)
	router.GET("", products.ListProduct)
}

func AdminRegister(router *gin.RouterGroup) {
	authGroup := router.Group("").Use(middlewares.AdminAuth())
	{
		// User
		authGroup.GET("users/:id/", adminUser.GetDetailUser)
		authGroup.GET("users/", adminUser.GetListUser)
		authGroup.PATCH("users/:id/", adminUser.DeleteUser)
		authGroup.PATCH("users/:id/block/", adminUser.BlockUser)
		authGroup.PATCH("users/:id/unblock/", adminUser.UnblockUser)
		// Category
		authGroup.GET("categories/", adminCategory.ListCategory)
		authGroup.GET("categories/:id/", adminCategory.GetDetail)
		authGroup.POST("categories/", adminCategory.Create)
		authGroup.PUT("categories/:id/", adminCategory.Update)
		authGroup.DELETE("categories/:id/", adminCategory.Delete)
		// Product
		authGroup.POST("products/", adminProduct.Create)
		authGroup.GET("products/", adminProduct.ListProduct)
		authGroup.GET("products/:id/", products.GetDetail)
		authGroup.PATCH("products/:id/", adminProduct.Update)
		authGroup.DELETE("products/:id/", adminProduct.Delete)
		// Order
		authGroup.GET("orders/", adminOrder.ListOrder)
		authGroup.GET("orders/:id/", adminOrder.GetDetail)
		authGroup.PATCH("orders/:id/", adminOrder.ChangeStatus)
	}

	// Admin
	router.POST("", admin.Create)
	router.POST("login/", admin.Login)
}
