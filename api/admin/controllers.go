package admin

import (
	"net/http"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func Create(c *gin.Context) {
	userModelValidator := NewAdminModelValidator()
	if err := userModelValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}

	user := userModelValidator.adminModel
	if err := models.SaveOneAdmin(&user); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}

	c.JSON(http.StatusCreated, gin.H{"message": "admin created"})
}

func Login(c *gin.Context) {
	userLoginValidator := NewAdminLoginValidator()
	if err := userLoginValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	user_request := userLoginValidator.adminModel
	current_user, check_exist := models.FindOneAdmin(&models.Admin{Username: user_request.Username})
	check_valid := current_user.CheckValidAuth(user_request.Password)
	if check_valid != nil || check_exist != nil {
		c.JSON(http.StatusUnauthorized, gin.H{"message": "Invalid Request"})
		return
	}
	serializer := AdminDetailSerializer{c, current_user}
	c.JSON(http.StatusCreated, gin.H{"user": serializer.LoginResponse()})
}
