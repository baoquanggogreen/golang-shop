package common

import (
	"mime/multipart"
	"os"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/joho/godotenv"
)

func InitSession() *s3manager.Uploader {
	sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_S3_REGION_NAME"))}))
	uploader := s3manager.NewUploader(sess, func(u *s3manager.Uploader) {
		u.BufferProvider = s3manager.NewBufferedReadSeekerWriteToPool(25 * 1024 * 1024)
	})
	return uploader
}

func UploadSingle(file *multipart.FileHeader) (string, error) {
	godotenv.Load(".env")
	var uploaded_url string
	var uploaded_err error

	src, _ := file.Open()
	defer src.Close()

	result, s3_err := InitSession().Upload(&s3manager.UploadInput{
		Bucket: aws.String(os.Getenv("AWS_STORAGE_BUCKET_NAME")),
		// Key:    aws.String(os.Getenv("AWS_SECRET_ACCESS_KEY")),
		// Key:    aws.String(os.Getenv("AWS_ACCESS_KEY_ID")),
		Key:  &file.Filename,
		Body: src,
	})
	uploaded_url = result.Location
	uploaded_err = s3_err
	return uploaded_url, uploaded_err
}

// func UploadMulti(files []multipart.FileHeader) ([]string, error) {
// 	for _, file := range files {
// 		src, _ := file.Open()
// 		defer src.Close()

// 		godotenv.Load(".env")
// 		var uploaded_url string
// 		var uploaded_err error
// 		sess := session.Must(session.NewSession(&aws.Config{Region: aws.String(os.Getenv("AWS_S3_REGION_NAME"))}))
// 		uploader := s3manager.NewUploader(sess, func(u *s3manager.Uploader) {
// 			u.BufferProvider = s3manager.NewBufferedReadSeekerWriteToPool(25 * 1024 * 1024)
// 		})

// 		result, s3_err := uploader.Upload(&s3manager.UploadInput{
// 			Bucket: aws.String(os.Getenv("AWS_STORAGE_BUCKET_NAME")),
// 			Key:    &file.Filename,
// 			Body:   src,
// 		})
// 	}
// 	uploaded_url = result.Location
// 	uploaded_err = s3_err
// 	return uploaded_url, uploaded_err
// }
