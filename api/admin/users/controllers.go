package admin

import (
	"net/http"
	"strconv"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func GetDetailUser(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	user, err := models.FindOneUser(&models.User{ID: id})
	serializer := UserDetailSerializer{c, user}
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	serializer = UserDetailSerializer{c, user}
	c.JSON(http.StatusOK, gin.H{"user": serializer.DetailResponse()})
}

func GetListUser(c *gin.Context) {
	listUserValidator := NewListUserValidator()
	paging, err := listUserValidator.Bind(c)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	users := models.GetListUser(&paging)
	serializer := ListUserSerializer{c, users}
	c.JSON(http.StatusOK, common.ListResponse(serializer.ListResponse(), paging))
}

func DeleteUser(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	err := models.DeleteOneUser(&models.User{ID: id})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Delete success"})
}

func BlockUser(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	err := models.BlockOneUser(&models.User{ID: id})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("validation", err))
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Block success"})
}

func UnblockUser(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	err := models.UnblockOneUser(&models.User{ID: id})
	if err != nil {
		c.JSON(http.StatusNotFound, common.NewError("validation", err))
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Unblock success"})
}
