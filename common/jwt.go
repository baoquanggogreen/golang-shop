package common

import (
	"errors"
	"github.com/dgrijalva/jwt-go"
	"github.com/joho/godotenv"
	"os"
)

type JWTClaim struct {
	UserID  uint `json:"user_id"`
	IsAdmin bool `json:"is_admin"`
	jwt.StandardClaims
}

type Authen struct {
	UserID  uint
	IsAdmin bool
}

func GenAuthToken(id uint, is_admin bool) string {
	godotenv.Load(".env")
	jwt_token := jwt.New(jwt.SigningMethodHS256)
	claims := jwt_token.Claims.(jwt.MapClaims)
	claims["user_id"] = id
	claims["is_admin"] = is_admin
	access_token, _ := jwt_token.SignedString([]byte(os.Getenv("SECRET_PASSWORD")))
	return access_token
}

func ValidateToken(authToken string) (user_id uint, is_admin bool, err error) {
	godotenv.Load(".env")
	token, err := jwt.ParseWithClaims(
		authToken,
		&JWTClaim{},
		func(token *jwt.Token) (interface{}, error) {
			return []byte(os.Getenv("SECRET_PASSWORD")), nil
		},
	)
	if err != nil {
		return
	}
	authen, ok := token.Claims.(*JWTClaim)
	if !ok {
		err = errors.New("token error")
		return
	}
	return authen.UserID, authen.IsAdmin, err
}
