package orders

import (
	"net/http"
	"strconv"
	"web-service-gin/common"
	"web-service-gin/middlewares"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func Create(c *gin.Context) {
	orderModelValidator := NewOrderModelValidator()
	if err := orderModelValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("validation", err))
		return
	}

	order_request := orderModelValidator.orderModel
	order, err := models.SaveOneOrder(&order_request)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	current_user, _ := models.FindOneUser(&models.User{ID: middlewares.GetCurrentUser(c)})
	serializer := OrderSerializer{c, order}
	response := serializer.DetailResponse()
	err_mail := common.SendOrderMail(
		current_user.Email,
		"Your order: ",
		"",
		response.Name,
		response.Status,
		response.PaymentStatus,
		response.OrderDate,
	)
	if err_mail != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"message": "Mail has not been sent"})
		return
	}
	c.JSON(http.StatusCreated, gin.H{"order": serializer.DetailResponse()})
}

func ListOrder(c *gin.Context) {
	listOrderValidator := NewListOrderValidator()
	paging, err := listOrderValidator.Bind(c)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	order_request := listOrderValidator.orderModel
	orders := models.GetListOrder(&order_request, &paging)
	serializer := ListOrderSerializer{c, orders}
	c.JSON(http.StatusOK, common.ListResponse(serializer.ListResponse(), paging))
}

func GetDetail(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	current_user := middlewares.GetCurrentUser(c)
	order, err := models.FindOneOrder(&models.Order{ID: id, UserID: current_user})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	serializer := OrderSerializer{c, order}
	c.JSON(http.StatusOK, gin.H{"order": serializer.DetailsResponse()})
}
