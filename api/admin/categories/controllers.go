package admin

import (
	"net/http"
	"strconv"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
)

func GetDetail(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	category, err := models.FindOneCategory(&models.Category{ID: id})
	serializer := CategoryDetailSerializer{c, category}
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	serializer = CategoryDetailSerializer{c, category}
	c.JSON(http.StatusOK, gin.H{"category": serializer.DetailResponse()})
}

func ListCategory(c *gin.Context) {
	listCategoryValidator := NewListCategoryValidator()
	paging, err := listCategoryValidator.Bind(c)
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	categories := models.GetListCategory(&paging)
	serializer := ListCategorySerializer{c, categories}
	c.JSON(http.StatusOK, common.ListResponse(serializer.ListResponse(), paging))
}

func Create(c *gin.Context) {
	addCategoryValidator := NewCategoryValidator()
	if err := addCategoryValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	category_request := addCategoryValidator.categoryModel
	if err := models.SaveOneCategory(&category_request); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	c.JSON(http.StatusCreated, gin.H{"message": "Category created"})
}

func Update(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	category, err := models.FindOneCategory(&models.Category{ID: id})
	if err != nil {
		c.JSON(http.StatusNotFound, gin.H{"message": "Not Found Error"})
		return
	}
	updateCategoryValidator := NewCategoryValidator()
	if err := updateCategoryValidator.Bind(c); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewValidatorError(err))
		return
	}
	category_request := updateCategoryValidator.categoryModel
	if err := models.UpdateCategory(category.ID, &category_request); err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Category updated"})
}

func Delete(c *gin.Context) {
	id64, _ := strconv.ParseUint(c.Param("id"), 10, 32)
	id := uint(id64)
	_, exist := models.FindOneCategory(&models.Category{ID: id})
	if exist != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", exist))
		return
	}
	err := models.DeleteCategory(&models.Category{ID: id})
	if err != nil {
		c.JSON(http.StatusUnprocessableEntity, common.NewError("database", err))
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Category deleted"})
}
