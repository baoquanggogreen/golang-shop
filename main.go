package main

import (
	"web-service-gin/config"
	"web-service-gin/models"
	"web-service-gin/routers"

	"gorm.io/gorm"
)

var (
	db *gorm.DB = config.ConnectDatabase()
)

func Migrate(db *gorm.DB) {
	checkTableExistAndMigrate(&models.User{})
	checkTableExistAndMigrate(&models.Cart{})
	checkTableExistAndMigrate(&models.Admin{})
	checkTableExistAndMigrate(&models.CartProduct{})
	checkTableExistAndMigrate(&models.Category{})
	checkTableExistAndMigrate(&models.Product{})
	checkTableExistAndMigrate(&models.Order{})
	checkTableExistAndMigrate(&models.OrderProduct{})
	checkTableExistAndMigrate(&models.Payment{})
	checkTableExistAndMigrate(&models.ProductCategory{})
	checkTableExistAndMigrate(&models.Picture{})
}

func checkTableExistAndMigrate(dst interface{}) {
	if !db.Migrator().HasTable(dst) {
		db.AutoMigrate(dst)
	} else {
		addColumn(&models.Product{}, "DeletedAt")
		addColumn(&models.CartProduct{}, "DeletedAt")
		addColumn(&models.ProductCategory{}, "DeletedAt")
		addColumn(&models.OrderProduct{}, "DeletedAt")
	}
}

func renameColumn(dst interface{}, oldName string, newName string) {
	if !db.Migrator().HasColumn(dst, oldName) {
		db.Migrator().RenameColumn(dst, oldName, newName)
	}
}

func addColumn(dst interface{}, fieldName string) {
	if !db.Migrator().HasColumn(dst, fieldName) {
		db.Migrator().AddColumn(dst, fieldName)
	}
}

func main() {
	defer config.CloseDatabaseConnection(db)

	config.ConnectDatabase()
	Migrate(db)
	router := routers.InitRouter()
	router.Run(":8000")
}
