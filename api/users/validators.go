package users

import (
	"errors"
	"os"
	"web-service-gin/common"
	"web-service-gin/models"

	"github.com/gin-gonic/gin"
	"github.com/joho/godotenv"
)

type UserModelValidator struct {
	FullName     string      `json:"full_name" binding:"required"`
	Username     string      `json:"username" binding:"required"`
	Email        string      `json:"email" binding:"required,email"`
	Password     string      `json:"password" binding:"required"`
	Phone        string      `json:"phone" binding:"required"`
	Birthday     string      `json:"birthday"`
	Gender       string      `json:"gender" binding:"required"`
	Address      string      `json:"address" binding:"required"`
	ActivateCode string      `json:"activatecode"`
	userModel    models.User `json:"-"`
}

type UserUpdateValidator struct {
	FullName  string      `json:"full_name" binding:"required"`
	Username  string      `json:"username" binding:"required"`
	Phone     string      `json:"phone" binding:"required"`
	Birthday  string      `json:"birthday"`
	Gender    string      `json:"gender" binding:"required"`
	Address   string      `json:"address" binding:"required"`
	userModel models.User `json:"-"`
}

type UserLoginValidator struct {
	Username  string      `json:"username" binding:"required"`
	Password  string      `json:"password" binding:"required"`
	userModel models.User `json:"-"`
}

type UserActiveValidator struct {
	ResetToken string      `json:"reset_token" binding:"required"`
	userModel  models.User `json:"-"`
}

type ResetPasswordEntryValidator struct {
	Email     string      `json:"email" binding:"required"`
	userModel models.User `json:"-"`
}

type ResetPasswordValidator struct {
	NewPassword    string      `json:"new_password" binding:"required"`
	RetypePassword string      `json:"retype_password" binding:"required"`
	ResetToken     string      `json:"reset_token" binding:"required"`
	userModel      models.User `json:"-"`
}

func (u *UserModelValidator) Bind(c *gin.Context) error {
	godotenv.Load(".env")
	err := common.Bind(c, u)
	if err != nil {
		return err
	}

	u.userModel.Username = u.Username
	u.userModel.Email = u.Email
	u.userModel.FullName = u.FullName
	u.userModel.Phone = u.Phone
	u.userModel.Birthday = u.Birthday
	u.userModel.Gender = u.Gender
	u.userModel.Address = u.Address

	if u.Password != os.Getenv("RANDOM_PASSWORD") {
		u.userModel.SetPassword(u.Password)
	}
	return nil
}

func (u *UserUpdateValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}

	u.userModel.Username = u.Username
	u.userModel.FullName = u.FullName
	u.userModel.Phone = u.Phone
	u.userModel.Birthday = u.Birthday
	u.userModel.Gender = u.Gender
	u.userModel.Address = u.Address
	return nil
}

func (u *UserLoginValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}
	u.userModel.Username = u.Username
	u.userModel.Password = u.Password
	return nil
}

func (u *UserActiveValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}
	u.userModel.ResetToken = u.ResetToken
	return nil
}

func (u *ResetPasswordEntryValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}
	u.userModel.Email = u.Email
	return nil
}

func (u *ResetPasswordValidator) Bind(c *gin.Context) error {
	err := common.Bind(c, u)
	if err != nil {
		return err
	}
	new_password := u.NewPassword
	retype_password := u.RetypePassword
	if new_password != retype_password {
		return errors.New("new password and confirm password not match")
	}
	u.userModel.SetPassword(new_password)
	u.userModel.ResetToken = ""
	return nil
}

// put default here
func NewUserModelValidator() UserModelValidator {
	userModelValidator := UserModelValidator{}
	return userModelValidator
}

func NewUserLoginValidator() UserLoginValidator {
	userLoginValidator := UserLoginValidator{}
	return userLoginValidator
}

func NewUserActiveCodeValidator() UserActiveValidator {
	userActiveCodeValidator := UserActiveValidator{}
	return userActiveCodeValidator
}

func NewResetPasswordEntryValidator() ResetPasswordEntryValidator {
	resetPasswordEntryValidator := ResetPasswordEntryValidator{}
	return resetPasswordEntryValidator
}

func NewResetPasswordValidator() ResetPasswordValidator {
	resetPasswordValidator := ResetPasswordValidator{}
	return resetPasswordValidator
}

func NewUserUpdateValidator() UserUpdateValidator {
	userUpdateValidator := UserUpdateValidator{}
	return userUpdateValidator
}
